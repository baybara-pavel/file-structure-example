# # Новая файловая структура

В качестве ориентира брал предыдущий опыт и [эту статью](https://hackernoon.com/the-100-correct-way-to-structure-a-react-app-or-why-theres-no-such-thing-3ede534ef1ed).
В примере старался быть последовательным и при этом не переусложнять.

Ключевые моменты:
1. Модульная структура. Модули лежат в папке `features`, шареные модули, если таковые будут - находятся в `core`. Эти папки введены, только потому что без них все же получается каша, хоть и плодится вложенность.

2. Модуль предлагается определять как сущность, которая либо содержит логику, либо содержит саброуты, либо запросы в бек, либо и то и другое. И еще одно грубое правило: если сущность содержит другие комопненты с логикой (модули) - то и сама сущность тоже модуль, даже если в ней самой логики нет. Модуль внутри себя содержит все что ему нужно: апи-запросы, сторы, роуты, специфичные комопненты, стили, а также уникальные константы\хелперы\утилиты

3. `features` могут содержать еще подмодули, создавать папку features для подмодулей - не нужно, так как файловая структура очень сильно перегружается

4. Общую структуру модулей и подмодулей стоит выдерживать максимально плоской и не плодить лишних папок, исключение для папок со сторами - все сторы луше сразу складывать в отдельную папку, так как это облегчит дальнейшую работу с ними, а также позволит удобнее работать с коллекциями сторов(см. далее). Если же количество файлов одного типа сущностей (стилей, роутов, констант и тд.) разростается, то стоит объединять их в папки с соответсвующим названием: `styles`, `routes` и тд. Прим: оговорка с папками в основном тут только из-за не оч. качественного начального дизайна сторов и местами странного разделения файлов со стилями. Может просто случиться так, что пока не будут переписаны сторы к идеально - плоской структуре в контексте фичи\модуля мы просто не сможем прийти

5. Файлы компонентов\Модулей должны быть названы также как и папка в которой они находятся. Так проще переходить сразу к нужному файлу + в табе сразу будет понятно с чем работаешь

6. Файл с константами модуля стоит называть `moduleName.constants.js`. Если файлов с константами модуля по какой-то причине несколько - объединяем их в папку `constants`

7. `Index.(ts|js)` рассматриваем модули `index` предлагаю не добавлять, так как это только все усложнит, а пути импорта к компонентам можно писать эффектино с помощью плагинов в IDE-ках и пофиг что там дважды будет повторяться имя

8. Используем только именованый экспорт

9. В папку `components` попадают все компоненты которые мы не можем определить как модуль. Варианты когда это комопонент:
- комопнент который сам не содержит сложной логики\стейта, или в него не вложены сложные компоненты;
- стейтлес компонент;
- стайлед компонент в отдельном файле(не используем постфикс styled и используем jsx расширение);
Желательно чтобы компоненты в этой папке были переиспользуемыми.

10. Файлы сторов принято называть по схеме `exampleStore.(js|ts)` - в случае если стор экспортирует экземпляр класса и `ExampleStore.(js|ts)` - если стор экспортирует сам класс.

11. Так как сторы при инжекте должны иметь уникальные имена, было решено мигрировать на скоупы(коллекции) сторов. Все сторы модуля и всех его сабмодулей импортируются в файл `exampleCollection.(js|ts)` в корне модуля, после чего объединяются в именованый скоуп, который уже экспортируется дальше. Все именованые скоупы, а также `sharedCollection` собираются вместе в файл в корне проекта `index.stores.(js\ts)`. Ключевая идея в том чтобы теперь инжектить не отдельные сторы, а коллекции сторов и в конечных местах обращаться к стору в стиле `exampleCollection.exampleStore`, либо использовать деструктуризацию. Для коллекций решено обязательно использовать постфикc `Collection`

12. Роуты модуля описываем в файле `moduleName.routes.(ts|js)` и складывать в корень модуля

13. Стили комопнент и стили модулей складываем рядом с файлом самого компонента, называть их `ComponentName.styles.(ts|js` и `ComponentName.scss`, чтобы не плодить вложенность на ровном месте. Если есть переиспользуемые внутри модуля стили, то их нужно хостить на уровне модуля в папке `styles` если это scss. В случае styled их желательно разбивать на отдельные файлы и хранить в папке `components`, как описано выше. Если же форс мажор и styled-стили неудобно разбить на разные файлы - их можно хранить в папке `styles`, в файле с названием модуля.

14. Пути импорта в конечных проектах описываем по схеме. Зависимости от root сущностей (констант, утилит, сторов и пр.) описываем через алиасы (`constants/`, `stores/` и тд.). Зависимости внутри модуля:
- от частей корневых сущностей главного родительского модуля - также через алиас путей `features/exampleModule/constants/`
- между саб модулями - через относительный путь `../../../stores`. В идеале, конечно таких путей должно быть минимум, но поскольку их не избежать - предлагается использовать именно такой способ.

15.  GraphQL запросы складываем рядом со сторами в которых эти запросы используются. По умолчанию все запросы складываем в файл `exampleStore.api.gql` и при необходимости (когда запросов становится через чур много) разделяем на два отдельных файла: на получение данных - query `exampleStore.query.gql` и изменение - mutation `exampleStore.mutation.gql`.
16. Обращения к апи выносим из сторов в отдельные файлы с именем `exampleStore.api.js`. Функции нормализации данных (приведения к формату) описываем на этом же уровне.
17. Стор + запросы + обращения к апи(+нормализация) объединяем в папки c названием стора `exampleStore`

18. Папку c тестами называем `__tests__` просто чтобы она всегда была наверху, а называть сами тесты по схеме : `entityName.test.js`. Также не стоит стесняться и просто брать и добавлять папку `__ tests__` там где это требуется, например в папки `helpers`, `utils` и тд.

19. Папку language удаляем как легаси. Ей на замену поднимаем i18n в корень
