import { exampleStore } from './stores/ExampleStore';
import { subModuleStore } from './SubModule/stores/SubModuleStore';

export const exampleCollection = {
  exampleStore,
  subModuleStore,
}
