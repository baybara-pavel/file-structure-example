import { sharedCollection } from './stores';
import { exampleCollection } from './features/Example/exampleCollection';

export const stores = {
  ...sharedCollection,
  exampleCollection,
};
